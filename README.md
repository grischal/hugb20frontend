# HUGB-303 Frontend #
This GitLab project will contain the different frontends for HUGB-303 in the fall semester 2020. Each frontend build is located in the corresponding build folder.

## Version: Master_s5_v5 ##
## Version History ##
### S5V5: 29th October 2020 ### 
- Added creation for candidates, regions, and elections to Akureyri, H1, H2.
- Added option to show region details and their polls for Akureyri, H1, H2
- Showing detailed information on the candidates when hovering over a plot (amount of details depends on section and implemented operations).

### S4V4: 21st October 2020 ### 
- Fixed issue #10 (Display errors for many elections)
- Added basic creation facilities for candidates and regions for Akureyri
- Added option to show region details and their polls (experimental) for HMV
- No major changes for H1 and H2, but preparations for coming release
- Added description below which operations are required for which section to fully support the frontend.

### S4V3: 14th October 2020 ### 
- Removed branches, instead created several build folders in master
- Fixed issues #5, #6, #7 (Accepting empty strings as return values, correcting display errors in polls)
- Adapted interfaces for all sections and added corresponding calls to Debug tabs.
- Added election/poll navigation to H1 and H2 (as in Aku/HMV). If elections are set for all polls, H1/H2 can now also display elections and show polls by election.

### S3V2: 7th October 2020 ### 
- Added Main page with basic visualisation
- For HMV and Akureyri, an overview of all elections is shown (via getAllElections).
- For H1 and H2, all polls are directly shown (via getAllPolls).
- The expected port for the backend can be changed by adding the port query. For instance, adding ?port=8123 will make calls to the backend at port 8123
- Debugging and error handling capabilities improved
- H2 getHistoricalPoll now sends a number parameter instead of a string

### S3V1: 30th September 2020 ### 
- Initial version
- Debug view only
- Limited debugging possibilities

## Requirements for Visualisation ##
The debug pages call all operations with basic, made-up parameters. However, for the remainder to work, the following operations are used:

### Akureyri ###
getAllElections, getPollsForElection, createCandidate, createRegion, createElection, getCandidateDetails, getPollPerRegion.
Additionally, the new methods getAllRegions and getAllCandidates are required for some functionality.

### HMV ###
getAllElections, getPollsForElection, getRegionDetails, getPollsPerRegion (No change in Sprint 5)

### H1 ###
getAllPolls, getElectableDetails, getLeadingElectableForRegion, createCandidate, createRegion, createElection, createParty.
Additionally, the new methods getAllCandidates, getAllParties, and getAllRegions are required for creation of candidates, parties and elections.

### H2 ###
getAllPolls, getCandidateDetails, getAllRegions, getRegionDetails, getPollPerRegion, getCandidatePolicy, createCandidate, createRegion, createElection.
Additionally, the new methods getAllCandidates, getAllParties, and getAllRegions are required for creation of candidates, parties and elections.

## Interface Descriptions ##
The interfaces for the four variants are described in the interface description files:
- [interface_h1.md](https://gitlab.com/grischal/hugb20frontend/-/blob/master/interface_h1.md)
- [interface_h2.md](https://gitlab.com/grischal/hugb20frontend/-/blob/master/interface_h2.md)
- [interface_akureyri.md](https://gitlab.com/grischal/hugb20frontend/-/blob/master/interface_akureyri.md)
- [interface_hmv.md](https://gitlab.com/grischal/hugb20frontend/-/blob/master/interface_hmv.md)

## Release Plan ##
No further releases are planned apart from minor bugfixes.

## Execution of the Frontend ##
The frontend is provided only as a built web application, without the source code being open (only the minified JS version). You execute the frontend by opening the `build[Section]/index.html` file in a browser (we tested Firefox and Chrome, but others should work).

The frontend assumes that your backend is listening on `ws://localhost:8080/` (Note: From version s3_v3 you can also provide an alternative port). This is the setting used in the Stream 05 example code.

To make sure you have the right version of the frontend, check the About tab in the application.

The frontend comes with a debug tab. There, we test the success cases with all methods. The return values are tested for compliance against our data model/schema. We do not test whether the ids or other content are appropriate. Similarly, we currently only call methods with ID parameters with the ID 1 (in all cases). More detailed testing might come later.