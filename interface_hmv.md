### Interface Description HMV ###

## Version ##
Current version: Hmv_s4_v3
Date: 7th October 2020

## Change log ##
- Hmv_s3_v1: Initial version.
- Hmv_s3_v2: Typo in Candidate ID fixed.
- Hmv_s4_v3: Added methods getSubgroupPolls, getAveragePoll. For getSubgroupPolls, added two new entitites to the data model: GroupPoll and SubgroupPoll. Added missing description to getAllRegions.

## Protocol ##
The frontend communicates in a client-server fashion with the backend, where the frontend is the client. That means the backend only answers, and never sends messages independently to the frontend.

1. For each message, the frontend establishes a new connection, sends the message, and closes the connection after receiving the message. This is already supported by the example code from Stream 05.
2. A correct message from the frontend to the backend is a JSON object that contains both a field called "op" and a field called "data". Additional fields can be ignored. The backend needs to be robust to wrongly-formatted messages (i.e., it shouldn't crash if something else is sent). An example for a correctly-formatted message would be: `{"op":"getAllElections", "data":{}}` (Attempting to call the operation "getAllElections" with no input parameters.
3. The data field in the input message contains all input parameters, following the names listed below. For instance, a correct call to "getPollsForElection" would need to have a string attribute called "electionID" in the data part of the message.
4. The server returns a JSON string which contains a single attribute called "msg". In case of an error, this field contains the error message. If the call was successul, it contains a JSON object according to the relevant output/return parameters of the operation. The output parameters are discussed below. For example, a successful call to "getPollsForElection" returns an array of Poll objects.
5. The frontend will make sure only correct return values are accepted. We will try to provide verbose error output, though (to help you debugging).

## Supported Operations ##
The following operations will be supported in Sprint 4. Note that the data model is described further down.

| Operation name | Input parameters | Output/return parameters | Description |
| ----------- | ----------- | ----------- | ----------- |
| **getAllElections** | None | [Election] | Returns an array of all existing elections in the system. If not election exists, an empty array is returned. |
| **getPollsForElection** | electionID:string | [Poll] (array of Polls) | Returns all polls existing in the system for a single election. If the election does not exist, an empty array is returned. |
| **getPollsPerRegion** | electionID:string, regionID:string | [Poll] | Returns all polls for a given region and election (i.e, the poll needs to fit both the election and the region). Returns an empty array if no polls exist for the region and/or election. |
| **getAllRegions** | electionID:string | [RegionDAO] | Returns an overview of all regions in an election. Returns an empty array if the election does not exist. |
| **getRegionDetails** | regionID:string | Region | Returns a single region with the given ID. Returns an empty string if no region with this ID exists. |
| **getSubgroupPolls** | pollID:string | [GroupPoll] | Returns an array of GroupPolls. Each GroupPoll object represents one way of breaking down the entire voting population into subgroups. E.g., one GroupPoll might include the different age sub-groups, another GroupPoll might divide the voters by ethnicity. Returns an empty array if the poll does not exist, or no grouping information exists for the poll.
| **getAveragePoll** | electionID:string | Poll | Returns a single Poll object that represents the average poll results of all polls for a single election. Returns an empty string if the election does not exist, or no poll exists for that election. |

## Data Model ##
The following data model is used for the return parameters. Please make sure to use the exact naming described below. Fields with an **(optional)** addition do not need to be present in the backend (i.e., the frontend does not complain if they're missing).

Poll:
- pollID: string
- dataArray: [RegionPoll]
- organization: string **(optional)**
- election: Election **(optional)**
- overallVotes: number **(optional)**

Describes an overall poll. The dataArray field contains the actual poll data.

RegionPoll:
- data: [PollData]
- region: Region **(optional)**

Describes a poll for a given region. In case regions are not implemented, this is basically just a wrapper around PollData, without any extra information.

PollData:
- candidate: Candidate
- votes: number

Describes the number of votes for a given candidate (in a poll/region).

Election:
- electionID: string
- name: string
- winner: string
- votingDate: string **(optional)**
- country: string  **(optional)**

Describes an election with its candidates and potentially the regions it covers. The winner is zero in case the election is not finished, otherwise it's the ID of the candidate who won.

Candidate:
- candidateID: string
- name: string
- birthDate: string **(optional)**
- politicalParty: string **(optional)**
- bio: string **(optional)**

Describes a candidate in detail. Most fields are left optional for now.

Region:
- regionID: string
- name: string
- code: string  **(optional)**
- registeredVoters: number **(optional)**
- residents: number **(optional)**

Describes a region by name and a number of optional meta data.

RegionDAO
- regionID: string
- name: string
- code: string  **(optional)**

A slimmed-down version of the Region without details.

GroupPoll
- groupID: string
- description: string
- subgroups: [SubgroupPoll]

A grouping of subgroups. For instance, one GroupPoll object might have the description "Grouping by age.", with the subgroups consisting of the different age groups "18-30", "31-45", "45-60", "60+".

SubgroupPoll
- subgroupID: string
- description: string
- poll: [RegionPoll]

A single subgroup belonging to a GroupPoll. Each subgroup has an array of RegionPolls, basically representing the polling results for that specific subgroup.