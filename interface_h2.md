### Interface Description H2 ###

## Version ##
Current version: H2_s5_v5
Date: 28th October 2020

## Change log ##
- H2_s3_v1: Initial version.
- H2_s3_v2: Typo in Candidate ID fixed.
- H2_s4_v3: Added methods getElectionDetails, getAllRegions, getAveragePoll, getPollPerElection. Added Election concept to the data model, including the electionSeats attribute.
- H2_s5_v4: Added methods getSubgroupPolls, getCandidatePolicy, createCandidate, createRegion, createElection, and createPoll. For getSubgroupPolls, added two new entitites to the data model: GroupPoll and SubgroupPoll.
- H2_s5_v5: Added getRegions, getAllElections.

## Protocol ##
The frontend communicates in a client-server fashion with the backend, where the frontend is the client. That means the backend only answers, and never sends messages independently to the frontend.

1. For each message, the frontend establishes a new connection, sends the message, and closes the connection after receiving the message. This is already supported by the example code from Stream 05.
2. A correct message from the frontend to the backend is a JSON object that contains both a field called "op" and a field called "data". Additional fields can be ignored. The backend needs to be robust to wrongly-formatted messages (i.e., it shouldn't crash if something else is sent). An example for a correctly-formatted message would be: `{"op":"getAllPolls", "data":{}}` (Attempting to call the operation "getAllPolls" with no input parameters.
3. The data field in the input message contains all input parameters, following the names listed below. For instance, a correct call to "getPollsForElection" would need to have a string attribute called "electionID" in the data part of the message.
4. The server returns a JSON string which contains a single attribute called "msg". In case of an error, this field contains the error message. If the call was successul, it contains a JSON object according to the relevant output/return parameters of the operation. The output parameters are discussed below. For example, a successful call to "getPollsForElection" returns an array of Poll objects.
5. The frontend will make sure only correct return values are accepted. We will try to provide verbose error output, though (to help you debugging).


## Supported Operations ##
The following operations will be supported in Sprint 3. Note that the data model is described further down.

| Operation name | Input parameters | Output/return parameters | Description |
| ----------- | ----------- | ----------- | ----------- |
| **getAllPolls** | None | [Poll] | Returns all polls existing in the system. If no poll exists, an empty array is returned. If you only support one poll, it's fine to return that one poll (as the only element in an array) |
| **getPollPerRegion** | regionID:string | [Poll] | Returns all polls for a given region. Returns an empty array if no polls exist for the region. |
| **getCandidates** | None | [CandidateDAO] | Returns overview of all candidates. |
| **getCandidateDetails** | candidateID:string | Candidate | Returns a single candidate with the given ID. Returns an empty string if no candidate with this ID exists. |
| **getRegionDetails** | regionID:string | Region | Returns a single region with the given ID. Returns an empty string if no region with this ID exists. |	
| **getHistoricalPolls**| year:number | [Poll] | Returns all polls from the specified year. If there are no polls in that year, an empty array is returned.
| **getElectionDetails** | electionID:string | Election | Returns a single election with the given ID. Returns an empty string if no election with this ID exists. |	
| **getAllRegions** | electionID:string | [Region] | Returns an overview of all regions in an election. Returns an empty array if the election does not exist. |
| **getAveragePoll** | electionID:string | Poll | Returns a single Poll object that represents the average poll results of all polls for a single election. Returns an empty string if the election does not exist, or no poll exists for that election. |
| **getPollPerElection** | electionID:string | [Poll] | Returns all polls for a given election. Returns an empty array if no polls exist for the election. |
| **getSubgroupPolls** | pollID:string | [GroupPoll] | Returns an array of GroupPolls. Each GroupPoll object represents one way of breaking down the entire voting population into subgroups. E.g., one GroupPoll might include the different age sub-groups, another GroupPoll might divide the voters by ethnicity. Returns an empty array if the poll does not exist, or no grouping information exists for the poll.|
| **getCandidatePolicy** | candidateID:string | string | Returns the textual policy of the give candidate. Returns an empty string if the candidate with the ID does not exist. |
| **createCandidate** | name: string, birthDate: string **(optional)**, politicalParty: string **(optional)**, bio: string **(optional)**, regionIDs: [string] **(optional)**, agenda: string **(optional)**, electionsWon: [string] **(optional)** | Candidate | Creates a new candidate. Upon success, the candidate is returned. Otherwise, an error message is returned. |
| **createRegion** | name: string, regionCode: string **(optional)**, population: number **(optional)**, registeredVoters: number **(optional)** | Region | Creates a new region. Upon success, the region is returned. Otherwise, an error message is returned. |
| **createElection** | name: string, candidateIDs: [string], votingDate: string **(optional)**, regionIDs : [string] **(optional)**, electionSeats: number **(optional)** | Election | Creates a new election. Upon success, the election is returned. Otherwise, an error message is returned. |
| **createPoll** | dataArray: [RegionPoll], organization: string **(optional)**, electionID: string **(optional)**, pollDate: string **(optional)**, url: string **(optional)** | Poll | Creates a new poll. Upon success, the poll is returned. Otherwise, an error message is returned. In the RegionPoll array, all objects with IDs are replaced with their ID. For instance, in a RegionPoll, the region attribute is provided as an ID instead of an object. This is also the case for nested objects. |
| **getRegions** | None | [Region] | Returns overview of all regions |
| **getAllElections** | None | [Election] | Returns overview of all elections |

## Data Model ##
The following data model is used for the return parameters. Please make sure to use the exact naming described below. Fields with an **(optional)** addition do not need to be present in the backend (i.e., the frontend does not complain if they're missing). This allows for a rather flexible implementation (e.g., completely skipping the elections for now).

Poll:
- pollID: string
- dataArray: [RegionPoll]
- year: number
- organization: string **(optional)**
- election: Election **(optional)**
- pollDate: string **(optional)**
- url: string **(optional)**

Describes an overall poll. The dataArray field contains the actual poll data.

RegionPoll:
- data: [PollData]
- region: Region **(optional)**

Describes a poll for a given region. In case regions are not implemented, this is basically just a wrapper around PollData, without any extra information.

PollData:
- candidate: Candidate
- votes: number

Describes the number of votes for a given candidate (in a poll/region).

Candidate:
- candidateID: string
- name: string
- birthDate: string **(optional)**
- politicalParty: string **(optional)**
- bio: string **(optional)**
- region: [Region] **(optional)**
- agenda: string **(optional)**
- electionsWon: [string]  **(optional)**

Describes a candidate in detail. Most fields are left optional for now. The region field is an array to support cross-regional elections, such as country-wide elections.

CandidateDAO:
- candidateID: string
- name: string
- politicalParty: string **(optional)**

A brief version of Candidate.

Region:
- regionID: string
- name: string
- regionCode: string **(optional)**
- population: number **(optional)**
- registeredVoters: number **(optional)**

Describes a region by name, and potentially the code (such as the US state codes), the population, and the amount of voters registered.

Election:
- electionID: string
- name: string
- candidates: [Candidate]
- votingDate: string **(optional)**
- regions : [Region] **(optional)**
- electionSeats: number **(optional)**

Describes an election with its candidates and potentially the regions it covers.

GroupPoll
- groupID: string
- description: string
- subgroups: [SubgroupPoll]

A grouping of subgroups. For instance, one GroupPoll object might have the description "Grouping by age.", with the subgroups consisting of the different age groups "18-30", "31-45", "45-60", "60+".

SubgroupPoll
- subgroupID: string
- description: string
- poll: [RegionPoll]

A single subgroup belonging to a GroupPoll. Each subgroup has an array of RegionPolls, basically representing the polling results for that specific subgroup.