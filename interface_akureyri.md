# Interface Description Akureyri #

## Version ##
Current version: Aku_s5_v5
Date: 28th October 2020

## Change log ##
- Aku_s3_v1: Initial version.
- Aku_s3_v2: Typo in Candidate ID fixed.
- Aku_s4_v3: Added getElectionDetails, getPollDetails, getAveragePoll, createCandidate, createRegion, createElection, and createPoll.
- Aku_s4_v4: Modified getAveragePoll to return a PollData array. Modified createPoll to expect nested IDs instead of nested objects in the [RegionPoll] array. Added electionType to Election entity.
- Aku_s5_v5: Added electionType as parameter to createElection. Added "get all methods": getAllCandidates, getAllRegions

## Protocol ##
The frontend communicates in a client-server fashion with the backend, where the frontend is the client. That means the backend only answers, and never sends messages independently to the frontend.

1. For each message, the frontend establishes a new connection, sends the message, and closes the connection after receiving the message. This is already supported by the example code from Stream 05.
2. A correct message from the frontend to the backend is a JSON object that contains both a field called "op" and a field called "data". Additional fields can be ignored. The backend needs to be robust to wrongly-formatted messages (i.e., it shouldn't crash if something else is sent). An example for a correctly-formatted message would be: `{"op":"getAllElections", "data":{}}` (Attempting to call the operation "getAllElections" with no input parameters.
3. The data field in the input message contains all input parameters, following the names listed below. For instance, a correct call to "getPollsForElection" would need to have a string attribute called "electionID" in the data part of the message.
4. The server returns a JSON string which contains a single attribute called "msg". In case of an error, this field contains the error message. If the call was successul, it contains a JSON object according to the relevant output/return parameters of the operation. The output parameters are discussed below. For example, a successful call to "getPollsForElection" returns an array of Poll objects.
5. The frontend will make sure only correct return values are accepted. We will try to provide verbose error output, though (to help you debugging).


## Supported Operations ##
The following operations will be supported in Sprint 3. Note that the data model is described further down.

| Operation name | Input parameters | Output/return parameters | Description |
| ----------- | ----------- | ----------- | ----------- |
| **getPollsForElection** | electionID:string | [Poll] (array of Polls) | Returns all polls existing in the system for a single election. If the election does not exist, an empty array is returned. |
| **getAllElections** | None | [Election] | Returns an array of all existing elections in the system. If not election exists, an empty array is returned. |
| **getPollPerRegion** | electionID:string, regionID:string | [Poll] | Returns all polls for a given region and election (i.e, the poll needs to fit both the election and the region). Returns an empty array if no polls exist for the region and/or election. |
| **getCandidates** | electionID:string | [Candidate] | Returns all candidates for a given election. Returns an empty array if no election with this ID exists, or the election has no candidate. |
| **getCandidateDetails** | candidateID:string | Candidate | Returns a single candidate with the given ID. Returns an empty string if no candidate with this ID exists. |
| **getRegionDetails** | regionID:string | Region | Returns a single region with the given ID. Returns an empty string if no region with this ID exists. |	
| **getElectionDetails** | electionID:string | Election | Returns a single election with the given ID. Returns an empty string if no election with this ID exists. |	
| **getPollDetails** | pollID:string | Poll | Returns a single poll with the given ID. Returns an empty string if no poll with this ID exists. |	
| **getAveragePoll** | electionID:string | [PollData] | Returns an array of PollData objects that represents the average poll results of all polls (of all regions) for a single election. Returns an empty array if the election does not exist, or no poll exists for that election. |
| **createCandidate** | name: string, birthDate: string **(optional)**, politicalParty: string **(optional)**, bio: string **(optional)** | Candidate | Creates a new candidate. Upon success, the candidate is returned. Otherwise, an error message is returned. |
| **createRegion** | name: string, registeredVoters: number **(optional)** | Region | Creates a new region. Upon success, the region is returned. Otherwise, an error message is returned. |
| **createElection** | name: string, candidateIDs: [string], votingDate: string **(optional)**, regionIDs : [string] **(optional)**, electionType: string **(optional)** | Election | Creates a new election. Upon success, the election is returned. Otherwise, an error message is returned. |
| **createPoll** | dataArray: [RegionPoll], organization: string **(optional)**, electionID: string **(optional)** | Poll | Creates a new poll. Upon success, the poll is returned. Otherwise, an error message is returned. In the RegionPoll array, all objects with IDs are replaced with their ID. For instance, in a RegionPoll, the region attribute is provided as an ID instead of an object. This is also the case for nested objects. |
| **getAllCandidates** | None | [Candidate] | Returns an array of all existing candidates in the system. If no candidate exists, an empty array is returned. |
| **getAllRegions** | None | [Region] | Returns an array of all existing regions in the system. If no region exists, an empty array is returned. |

## Data Model ##
The following data model is used for the return parameters. Please make sure to use the exact naming described below. Fields with an **(optional)** addition do not need to be present in the backend (i.e., the frontend does not complain if they're missing).

Poll:
- pollID: string
- dataArray: [RegionPoll]
- organization: string **(optional)**
- election: Election **(optional)**

Describes an overall poll. The dataArray field contains the actual poll data.

RegionPoll:
- data: [PollData]
- region: Region **(optional)**

Describes a poll for a given region. In case regions are not implemented, this is basically just a wrapper around PollData, without any extra information.

PollData:
- candidate: Candidate
- votes: number

Describes the number of votes for a given candidate (in a poll/region).

Election:
- electionID: string
- name: string
- candidates: [Candidate]
- votingDate: string **(optional)**
- regions: [Region] **(optional)**
- electionType: string **(optional)**

Describes an election with its candidates and potentially the regions it covers.

Candidate:
- candidateID: string
- name: string
- birthDate: string **(optional)**
- politicalParty: string **(optional)**
- bio: string **(optional)**

Describes a candidate in detail. Most fields are left optional for now.

Region:
- regionID: string
- name: string
- registeredVoters: number **(optional)**

Describes a region by name and potentially the amount of voters registered.
