### Interface Description H1 ###

## Version ##
Current version: H1_s5_v6
Date: 28th October 2020

## Change log ##
- H1_s3_v1: Initial version.
- H1_s3_v2: Typo in Candidate ID fixed.
- H1_s4_v3: Introduced the concept of "electables" instead of only candidates. The existing data model and return types are adapted accordingly. Added methods getHistoricalPollsForElection, getLeadingElectableForRegion, and getAveragePoll.
- H1_s4_v4: Fixed input parameter for getCandidateDetails to be electableID.
- H1_s5_v5: Changed getCandidates to getElectables, and getCandidateDetails to getElectableDetails. Added methods createElection, createCandidate, createParty, createRegion, and createPoll. Finally, added a success flag to the return string.
- H1_s5_v6: Added "get all" operations getAllCandidates, getAllParties, getAllRegions.

## Protocol ##
The frontend communicates in a client-server fashion with the backend, where the frontend is the client. That means the backend only answers, and never sends messages independently to the frontend.

1. For each message, the frontend establishes a new connection, sends the message, and closes the connection after receiving the message. This is already supported by the example code from Stream 05.
2. A correct message from the frontend to the backend is a JSON object that contains both a field called "op" and a field called "data". Additional fields can be ignored. The backend needs to be robust to wrongly-formatted messages (i.e., it shouldn't crash if something else is sent). An example for a correctly-formatted message would be: `{"op":"getAllPolls", "data":{}}` (Attempting to call the operation "getAllPolls" with no input parameters.
3. The data field in the input message contains all input parameters, following the names listed below. For instance, a correct call to "getPollsForElection" would need to have a string attribute called "electionID" in the data part of the message.
4. The server returns a JSON string which contains an attribute called "msg", and an optional attribute called "success". In case of an error, the **msg** field contains the error message. If the call was successful, it contains a JSON object according to the relevant output/return parameters of the operation. The output parameters are discussed below. For example, a successful call to "getPollsForElection" returns an array of Poll objects. If present, the **success** field contains a C-style (numerical) return code, with 0 indicating success, and non-zero indicating failure.
5. The frontend will make sure only correct return values are accepted. We will try to provide verbose error output, though (to help you debugging).


## Supported Operations ##
The following operations will be supported in Sprint 3. Note that the data model is described further down.

| Operation name | Input parameters | Output/return parameters | Description |
| ----------- | ----------- | ----------- | ----------- |
| **getAllPolls** | None | [Poll] | Returns all polls existing in the system. If no poll exists, an empty array is returned. If you only support one poll, it's fine to return that one poll (as the only element in an array) |
| **getPollPerRegion** | electionID:string, regionID:string | [Poll] | Returns all polls for a given region and election (i.e, the poll needs to fit both the election and the region). Returns an empty array if no polls exist for the region and/or election. |
| **getElectables** | electionID:string | [Electable] | Returns all electables for a given election. Returns an empty array if no election with this ID exists, or the election has no electable. |
| **getElectableDetails** | electableID:string | Electable | Returns a single electable with the given ID. Returns an empty string if no electable with this ID exists. |
| **getRegionDetails** | regionID:string | Region | Returns a single region with the given ID. Returns an empty string if no region with this ID exists. |	
| **getHistoricalPollsForElection** | electionID:string, dateAfter: string, dateBefore: string | [Poll] | Returns all polls for a given election that have their startDate after **dateAfter** and their endDate before **dateBefore**. Returns an empty array if no polls exist for the election in the given time frame. The dateAfter and dateBefore parameters are expected to be formatted in ISO format "YYYY-MM-DD", e.g., "2020-01-15" for the 15th January 2020. |
| **getAveragePoll** | electionID:string | Poll | Returns a single Poll object that represents the average poll results of all polls for a single election. Returns an empty string if the election does not exist, or no poll exists for that election. |
| **getLeadingElectableForRegion** | electionID:string, regionID:string | PollData | Returns the poll data for the electable who is leading in the given region and election. Returns an empty string if the election does not exist, or the region does not exist. |
| **createElection** | name: string, candidateIDs: [string], votingDate: string **(optional)**, regionIDs : [string] **(optional)** | Election | Creates a new election. Upon success, the election is returned. Otherwise, an error message is returned. |
| **createCandidate** | name: string, description:string **(optional)**, regionIDs: [string] **(optional)**, birthDate: string **(optional)**, partyID: string **(optional)** | Candidate | Creates a new candidate. Upon success, the candidate is returned. Otherwise, an error message is returned. |
| **createParty** | name: string, description:string **(optional)**, regionIDs: [string] **(optional)**, foundedDate: string **(optional)** | Party | Creates a new party. Upon success, the party is returned. Otherwise, an error message is returned. |
| **createPoll** | dataArray: [RegionPoll], organization: string **(optional)**, electionID: string **(optional)** startDate: string **(optional)**, endDate: string **(optional)**, country: string **(optional)**| Poll | Creates a new poll. Upon success, the poll is returned. Otherwise, an error message is returned. In the RegionPoll array, all objects with IDs are replaced with their ID. For instance, in a RegionPoll, the region attribute is provided as an ID instead of an object. This is also the case for nested objects. |
| **createRegion** | name: string, regionCode: string **(optional)**, population: number **(optional)**, registeredVoters: number **(optional)** | Region | Creates a new region. Upon success, the region is returned. Otherwise, an error message is returned. |
| **getAllCandidates** | None | [Candidate] | Returns all candidates. Returns an empty array if no candidate exists. |
| **getAllRegions** | None | [Region] | Returns all regions. Returns an empty array if no region exists. |
| **getAllParties** | None | [Party] | Returns all parties. Returns an empty array if no political party exists. |

## Data Model ##
The following data model is used for the return parameters. Please make sure to use the exact naming described below. Fields with an **(optional)** addition do not need to be present in the backend (i.e., the frontend does not complain if they're missing). This allows for a rather flexible implementation (e.g., completely skipping the elections for now).

Poll:
- pollID: string
- dataArray: [RegionPoll]
- organization: string **(optional)**
- election: Election **(optional)**
- startDate: string **(optional)**
- endDate: string **(optional)**
- country: string **(optional)**

Describes an overall poll. The dataArray field contains the actual poll data.

RegionPoll:
- data: [PollData]
- region: Region **(optional)**

Describes a poll for a given region. In case regions are not implemented, this is basically just a wrapper around PollData, without any extra information.

PollData:
- candidate: Electable
- votes: number

Describes the number of votes for a given electable (in a poll/region).

Election:
- electionID: string
- name: string
- candidates: [Electable]
- votingDate: string **(optional)**
- regions : [Region] **(optional)**

Describes an election with its candidates and potentially the regions it covers.

Electable:
- electableID: string
- name: string
- description: string **(optional)**
- region: [Region]  **(optional)**

An abstract "electable", which can either be a party or a candidate (see below). Serves mainly as a way to support both candidate and party elections. Most fields are left optional for now. The region field is an array to support cross-regional elections, such as country-wide elections.

Party **(is an Electable)**:
- foundedDate: string **(optional)**

A political party. The party inherits all attributes from Electable.

Candidate **(is an Electable)**:
- birthDate: string **(optional)**
- politicalParty: Party **(optional)**

A candidate (person). Can belong to a political party.  The candidate inherits all attributes from Electable.

Region:
- regionID: string
- name: string
- regionCode: string **(optional)**
- population: number **(optional)**
- registeredVoters: number **(optional)**

Describes a region by name, and potentially the code (such as the US state codes), the population, and the amount of voters registered.
